
<!DOCTYPE html>

<html lang="en">
<html>

<head>
    <meta charset="utf-8">
    <script type="text/javascript">
    if (parent.frames.length !== 0) {
        top.location = '<?=admin_url()?>';
    }
    </script>
    <script src="<?= base_url().'assets/register/' ?>js/respond.min.js"></script>
    <link rel="stylesheet" href="<?= base_url().'assets/register/' ?>AdminLTE/plugins/pace/pace.css?v=36">

<!-- Font Awesome -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>plugins/font-awesome/css/font-awesome.min.css?v=36">

<!-- Styles -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>plugins/jquery-ui/jquery-ui.min.css?v=36">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>bootstrap/css/bootstrap.min.css?v=36">

<!-- @if( in_array(session()->get('user.language', config('app.locale')), config('constants.langs_rtl')) ) -->
	<!-- <link rel="stylesheet" href="<?= base_url().'assets/register/' ?>bootstrap/css/bootstrap.rtl.min.css?v=36"> -->
<!-- @endif -->
  
<!-- Ionicons -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>plugins/ionicons/css/ionicons.min.css?v=36">
 <!-- Select2 -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>AdminLTE/plugins/select2/select2.min.css?v=36">
<!-- Theme style -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>AdminLTE/css/AdminLTE.min.css?v=36">
<!-- iCheck -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>AdminLTE/plugins/iCheck/square/blue.css?v=36">

<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>AdminLTE/plugins/datepicker/bootstrap-datepicker.min.css?v=36">

<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>AdminLTE/plugins/DataTables/datatables.min.css?v=36">

<!-- Toastr -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>plugins/toastr/toastr.min.css?v=36">
<!-- Bootstrap file input -->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>plugins/bootstrap-fileinput/fileinput.min.css?v=36">

<!-- AdminLTE Skins.-->
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>AdminLTE/css/skins/_all-skins.min.css?v=36">

<!-- @if( in_array(session()->get('user.language', config('app.locale')), config('constants.langs_rtl')) ) -->
	<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>AdminLTE/css/AdminLTE.rtl.min.css?v=36">
<!-- @endif -->

<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>AdminLTE/plugins/daterangepicker/daterangepicker.css?v=36">
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>plugins/bootstrap-tour/bootstrap-tour.min.css?v=36">
<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>plugins/calculator/calculator.css?v=36">

<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css?v=36">

<link rel="stylesheet" href="<?= base_url().'assets/register/' ?>css/app.css?v=36">
    <!-- Jquery Steps -->
    <link rel="stylesheet" href="<?= base_url().'assets/register/' ?>plugins/jquery.steps/jquery.steps.css?v=36">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
body{
    align-items: center;
    flex-direction: column;
    justify-content: center;
    background-size: cover !important;
    background-position: center !important;
    background-image: url(<?= base_url().'assets/register/img' ?>/1.jpg) !important;
}
</style>
</head>

<body class="hold-transition register-page">
    <div class="container-fluid mt-30">

        <!-- Language changer -->
        <div class="row">
            <div class="col-md-6">
                <div class="pull-left mt-10">
                    <!-- <select class="form-control input-sm" id="change_lang">
                        <option value="en" selected>
                            English
                        </option>
                        <option value="es">
                            Spanish - Espa�ol
                        </option>
                        <option value="sq">
                            Albanian - Shqip
                        </option>
                        <option value="hi">
                            Hindi - ?????
                        </option>
                        <option value="nl">
                            Dutch
                        </option>
                        <option value="fr">
                            French - Fran�ais
                        </option>
                        <option value="de">
                            German - Deutsch
                        </option>
                        <option value="ar">
                            Arabic - ????????????
                        </option>
                        <option value="tr">
                            Turkish - T�rk�e
                        </option>
                        <option value="id">
                            Indonesian
                        </option>
                        <option value="ps">
                            Pashto
                        </option>
                        <option value="pt">
                            Portuguese
                        </option>
                    </select> -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="pull-right">

                </div>
            </div>
        </div>

        <div class="row text-center" style="margin-top:50px;">
            <h1 class="text-center page-header " style="border: none;
    font-size: 40px;"></h1>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box box-solid">
                            <div class="box-header with-border">
                                <h3 class="box-title text-center">Register and Get Started in minutes</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <?php $attrib = ['id' => 'business_register_form'];
                                echo admin_form_open('auth/submit', $attrib);
                                ?> <h3>Business</h3>
                                <input name="language" type="hidden">

                                <fieldset>
                                    <legend>Business details:</legend>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Business Name:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-suitcase"></i>
                                                </span>
                                                <input class="form-control" placeholder="Business Name" required
                                                    name="name" type="text" id="name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="start_date">Start Date:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <input class="form-control start-date-picker" placeholder="Start Date"
                                                    readonly name="start_date" type="text" id="start_date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="currency_id">Currency:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-money"></i>
                                                </span>
                                                <select class="form-control select2_register" required id="currency_id"
                                                    name="currency_id">
                                                    <option selected="selected" value="">Select Currency</option>
                                                    <option value="AF">Afghanistan - Afghanis(AF) </option>
                                                    <option value="ALL">Albania - Leke(ALL) </option>
                                                    <option value="DZD">Algerie - Algerian dinar(DZD) </option>
                                                    <option value="USD">America - Dollars(USD) </option>
                                                    <option value="ARS">Argentina - Pesos(ARS) </option>
                                                    <option value="AWG">Aruba - Guilders(AWG) </option>
                                                    <option value="AUD">Australia - Dollars(AUD) </option>
                                                    <option value="AZ">Azerbaijan - New Manats(AZ) </option>
                                                    <option value="BSD">Bahamas - Dollars(BSD) </option>
                                                    <option value="BDT">Bangladesh - Taka(BDT) </option>
                                                    <option value="BBD">Barbados - Dollars(BBD) </option>
                                                    <option value="BYR">Belarus - Rubles(BYR) </option>
                                                    <option value="EUR">Belgium - Euro(EUR) </option>
                                                    <option value="BZD">Beliz - Dollars(BZD) </option>
                                                    <option value="BMD">Bermuda - Dollars(BMD) </option>
                                                    <option value="BOB">Bolivia - Bolivianos(BOB) </option>
                                                    <option value="BAM">Bosnia and Herzegovina - Convertible Marka(BAM)
                                                    </option>
                                                    <option value="BWP">Botswana - Pula&#039;s(BWP) </option>
                                                    <option value="BRL">Brazil - Reais(BRL) </option>
                                                    <option value="GBP">Britain [United Kingdom] - Pounds(GBP) </option>
                                                    <option value="BND">Brunei Darussalam - Dollars(BND) </option>
                                                    <option value="BG">Bulgaria - Leva(BG) </option>
                                                    <option value="KHR">Cambodia - Riels(KHR) </option>
                                                    <option value="CAD">Canada - Dollars(CAD) </option>
                                                    <option value="KYD">Cayman Islands - Dollars(KYD) </option>
                                                    <option value="CLP">Chile - Pesos(CLP) </option>
                                                    <option value="CNY">China - Yuan Renminbi(CNY) </option>
                                                    <option value="COP">Colombia - Pesos(COP) </option>
                                                    <option value="CRC">Costa Rica - Col�n(CRC) </option>
                                                    <option value="HRK">Croatia - Kuna(HRK) </option>
                                                    <option value="CUP">Cuba - Pesos(CUP) </option>
                                                    <option value="EUR">Cyprus - Euro(EUR) </option>
                                                    <option value="CZK">Czech Republic - Koruny(CZK) </option>
                                                    <option value="DKK">Denmark - Kroner(DKK) </option>
                                                    <option value="DOP">Dominican Republic - Pesos(DOP ) </option>
                                                    <option value="XCD">East Caribbean - Dollars(XCD) </option>
                                                    <option value="EGP">Egypt - Pounds(EGP) </option>
                                                    <option value="SVC">El Salvador - Colones(SVC) </option>
                                                    <option value="GBP">England [United Kingdom] - Pounds(GBP) </option>
                                                    <option value="EUR">Euro - Euro(EUR) </option>
                                                    <option value="FKP">Falkland Islands - Pounds(FKP) </option>
                                                    <option value="FJD">Fiji - Dollars(FJD) </option>
                                                    <option value="EUR">France - Euro(EUR) </option>
                                                    <option value="GHC">Ghana - Cedis(GHC) </option>
                                                    <option value="GIP">Gibraltar - Pounds(GIP) </option>
                                                    <option value="EUR">Greece - Euro(EUR) </option>
                                                    <option value="GTQ">Guatemala - Quetzales(GTQ) </option>
                                                    <option value="GGP">Guernsey - Pounds(GGP) </option>
                                                    <option value="GYD">Guyana - Dollars(GYD) </option>
                                                    <option value="EUR">Holland [Netherlands] - Euro(EUR) </option>
                                                    <option value="HNL">Honduras - Lempiras(HNL) </option>
                                                    <option value="HKD">Hong Kong - Dollars(HKD) </option>
                                                    <option value="HUF">Hungary - Forint(HUF) </option>
                                                    <option value="ISK">Iceland - Kronur(ISK) </option>
                                                    <option value="INR">India - Rupees(INR) </option>
                                                    <option value="IDR">Indonesia - Rupiahs(IDR) </option>
                                                    <option value="IRR">Iran - Rials(IRR) </option>
                                                    <option value="IQD">Iraq - Iraqi dinar(IQD) </option>
                                                    <option value="EUR">Ireland - Euro(EUR) </option>
                                                    <option value="IMP">Isle of Man - Pounds(IMP) </option>
                                                    <option value="ILS">Israel - New Shekels(ILS) </option>
                                                    <option value="EUR">Italy - Euro(EUR) </option>
                                                    <option value="JMD">Jamaica - Dollars(JMD) </option>
                                                    <option value="JPY">Japan - Yen(JPY) </option>
                                                    <option value="JEP">Jersey - Pounds(JEP) </option>
                                                    <option value="KZT">Kazakhstan - Tenge(KZT) </option>
                                                    <option value="KES">Kenya - Kenyan shilling(KES) </option>
                                                    <option value="KPW">Korea [North] - Won(KPW) </option>
                                                    <option value="KRW">Korea [South] - Won(KRW) </option>
                                                    <option value="KGS">Kyrgyzstan - Soms(KGS) </option>
                                                    <option value="LAK">Laos - Kips(LAK) </option>
                                                    <option value="LVL">Latvia - Lati(LVL) </option>
                                                    <option value="LBP">Lebanon - Pounds(LBP) </option>
                                                    <option value="LRD">Liberia - Dollars(LRD) </option>
                                                    <option value="CHF">Liechtenstein - Switzerland Francs(CHF)
                                                    </option>
                                                    <option value="LTL">Lithuania - Litai(LTL) </option>
                                                    <option value="EUR">Luxembourg - Euro(EUR) </option>
                                                    <option value="MKD">Macedonia - Denars(MKD) </option>
                                                    <option value="MYR">Malaysia - Ringgits(MYR) </option>
                                                    <option value="EUR">Malta - Euro(EUR) </option>
                                                    <option value="MUR">Mauritius - Rupees(MUR) </option>
                                                    <option value="MX">Mexico - Pesos(MX) </option>
                                                    <option value="MNT">Mongolia - Tugriks(MNT) </option>
                                                    <option value="MZ">Mozambique - Meticais(MZ) </option>
                                                    <option value="NAD">Namibia - Dollars(NAD) </option>
                                                    <option value="NPR">Nepal - Rupees(NPR) </option>
                                                    <option value="EUR">Netherlands - Euro(EUR) </option>
                                                    <option value="ANG">Netherlands Antilles - Guilders(ANG) </option>
                                                    <option value="NZD">New Zealand - Dollars(NZD) </option>
                                                    <option value="NIO">Nicaragua - Cordobas(NIO) </option>
                                                    <option value="NG">Nigeria - Nairas(NG) </option>
                                                    <option value="KPW">North Korea - Won(KPW) </option>
                                                    <option value="NOK">Norway - Krone(NOK) </option>
                                                    <option value="OMR">Oman - Rials(OMR) </option>
                                                    <option value="PKR">Pakistan - Rupees(PKR) </option>
                                                    <option value="PAB">Panama - Balboa(PAB) </option>
                                                    <option value="PYG">Paraguay - Guarani(PYG) </option>
                                                    <option value="PE">Peru - Nuevos Soles(PE) </option>
                                                    <option value="PHP">Philippines - Pesos(PHP) </option>
                                                    <option value="PL">Poland - Zlotych(PL) </option>
                                                    <option value="QAR">Qatar - Rials(QAR) </option>
                                                    <option value="RO">Romania - New Lei(RO) </option>
                                                    <option value="RUB">Russia - Rubles(RUB) </option>
                                                    <option value="SHP">Saint Helena - Pounds(SHP) </option>
                                                    <option value="SAR">Saudi Arabia - Riyals(SAR) </option>
                                                    <option value="RSD">Serbia - Dinars(RSD) </option>
                                                    <option value="SCR">Seychelles - Rupees(SCR) </option>
                                                    <option value="SGD">Singapore - Dollars(SGD) </option>
                                                    <option value="EUR">Slovenia - Euro(EUR) </option>
                                                    <option value="SBD">Solomon Islands - Dollars(SBD) </option>
                                                    <option value="SOS">Somalia - Shillings(SOS) </option>
                                                    <option value="ZAR">South Africa - Rand(ZAR) </option>
                                                    <option value="KRW">South Korea - Won(KRW) </option>
                                                    <option value="EUR">Spain - Euro(EUR) </option>
                                                    <option value="LKR">Sri Lanka - Rupees(LKR) </option>
                                                    <option value="SRD">Suriname - Dollars(SRD) </option>
                                                    <option value="SEK">Sweden - Kronor(SEK) </option>
                                                    <option value="CHF">Switzerland - Francs(CHF) </option>
                                                    <option value="SYP">Syria - Pounds(SYP) </option>
                                                    <option value="TWD">Taiwan - New Dollars(TWD) </option>
                                                    <option value="TZS">Tanzania - Tanzanian shilling(TZS) </option>
                                                    <option value="THB">Thailand - Baht(THB) </option>
                                                    <option value="TTD">Trinidad and Tobago - Dollars(TTD) </option>
                                                    <option value="TRY">Turkey - Lira(TRY) </option>
                                                    <option value="TRL">Turkey - Liras(TRL) </option>
                                                    <option value="TVD">Tuvalu - Dollars(TVD) </option>
                                                    <option value="UGX">Uganda - Uganda shillings(UGX) </option>
                                                    <option value="UAH">Ukraine - Hryvnia(UAH) </option>
                                                    <option value="AED">United Arab Emirates - United Arab Emirates
                                                        dirham(AED) </option>
                                                    <option value="GBP">United Kingdom - Pounds(GBP) </option>
                                                    <option value="USD">United States of America - Dollars(USD)
                                                    </option>
                                                    <option value="UYU">Uruguay - Pesos(UYU) </option>
                                                    <option value="UZS">Uzbekistan - Sums(UZS) </option>
                                                    <option value="EUR">Vatican City - Euro(EUR) </option>
                                                    <option value="VEF">Venezuela - Bolivares Fuertes(VEF) </option>
                                                    <option value="VND">Vietnam - Dong(VND) </option>
                                                    <option value="YER">Yemen - Rials(YER) </option>
                                                    <option value="ZWD">Zimbabwe - Zimbabwe Dollars(ZWD) </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="business_logo">Upload Logo:</label>
                                            <input accept="image/*" name="business_logo" type="file" id="business_logo">
                                        </div>
                                    </div> -->
                                    <!-- <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="website">Website:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-globe"></i>
                                                </span>
                                                <input class="form-control" placeholder="Website" name="website"
                                                    type="text" id="website">
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="mobile">Business contact number:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                                <input class="form-control" placeholder="Business contact number"
                                                    name="mobile" type="text" id="mobile">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="alternate_number">Alternate contact number:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-phone"></i>
                                                </span>
                                                <input class="form-control" placeholder="Alternate contact number"
                                                    name="alternate_number" type="text" id="alternate_number">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country">Company:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-globe"></i>
                                                </span>
                                                <input class="form-control" placeholder="Company" required
                                                    name="country" type="text" id="country">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="state">State:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker"></i>
                                                </span>
                                                <input class="form-control" placeholder="State" required name="state"
                                                    type="text" id="state">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="city">City:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker"></i>
                                                </span>
                                                <input class="form-control" placeholder="City" required name="city"
                                                    type="text" id="city">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="zip_code">Zip Code:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker"></i>
                                                </span>
                                                <input class="form-control" placeholder="Zip/Postal Code" required
                                                    name="zip_code" type="text" id="zip_code">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="landmark">Landmark:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-map-marker"></i>
                                                </span>
                                                <input class="form-control" placeholder="Landmark" required
                                                    name="landmark" type="text" id="landmark">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="time_zone">Time zone:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </span>
                                                <select class="form-control select2_register" required id="time_zone"
                                                    name="time_zone">
                                                    <option value="">Time zone</option>
                                                    <option value="Africa/Abidjan">Africa/Abidjan</option>
                                                    <option value="Africa/Accra">Africa/Accra</option>
                                                    <option value="Africa/Addis_Ababa">Africa/Addis_Ababa</option>
                                                    <option value="Africa/Algiers">Africa/Algiers</option>
                                                    <option value="Africa/Asmara">Africa/Asmara</option>
                                                    <option value="Africa/Bamako">Africa/Bamako</option>
                                                    <option value="Africa/Bangui">Africa/Bangui</option>
                                                    <option value="Africa/Banjul">Africa/Banjul</option>
                                                    <option value="Africa/Bissau">Africa/Bissau</option>
                                                    <option value="Africa/Blantyre">Africa/Blantyre</option>
                                                    <option value="Africa/Brazzaville">Africa/Brazzaville</option>
                                                    <option value="Africa/Bujumbura">Africa/Bujumbura</option>
                                                    <option value="Africa/Cairo">Africa/Cairo</option>
                                                    <option value="Africa/Casablanca">Africa/Casablanca</option>
                                                    <option value="Africa/Ceuta">Africa/Ceuta</option>
                                                    <option value="Africa/Conakry">Africa/Conakry</option>
                                                    <option value="Africa/Dakar">Africa/Dakar</option>
                                                    <option value="Africa/Dar_es_Salaam">Africa/Dar_es_Salaam</option>
                                                    <option value="Africa/Djibouti">Africa/Djibouti</option>
                                                    <option value="Africa/Douala">Africa/Douala</option>
                                                    <option value="Africa/El_Aaiun">Africa/El_Aaiun</option>
                                                    <option value="Africa/Freetown">Africa/Freetown</option>
                                                    <option value="Africa/Gaborone">Africa/Gaborone</option>
                                                    <option value="Africa/Harare">Africa/Harare</option>
                                                    <option value="Africa/Johannesburg">Africa/Johannesburg</option>
                                                    <option value="Africa/Juba">Africa/Juba</option>
                                                    <option value="Africa/Kampala">Africa/Kampala</option>
                                                    <option value="Africa/Khartoum">Africa/Khartoum</option>
                                                    <option value="Africa/Kigali">Africa/Kigali</option>
                                                    <option value="Africa/Kinshasa">Africa/Kinshasa</option>
                                                    <option value="Africa/Lagos">Africa/Lagos</option>
                                                    <option value="Africa/Libreville">Africa/Libreville</option>
                                                    <option value="Africa/Lome">Africa/Lome</option>
                                                    <option value="Africa/Luanda">Africa/Luanda</option>
                                                    <option value="Africa/Lubumbashi">Africa/Lubumbashi</option>
                                                    <option value="Africa/Lusaka">Africa/Lusaka</option>
                                                    <option value="Africa/Malabo">Africa/Malabo</option>
                                                    <option value="Africa/Maputo">Africa/Maputo</option>
                                                    <option value="Africa/Maseru">Africa/Maseru</option>
                                                    <option value="Africa/Mbabane">Africa/Mbabane</option>
                                                    <option value="Africa/Mogadishu">Africa/Mogadishu</option>
                                                    <option value="Africa/Monrovia">Africa/Monrovia</option>
                                                    <option value="Africa/Nairobi">Africa/Nairobi</option>
                                                    <option value="Africa/Ndjamena">Africa/Ndjamena</option>
                                                    <option value="Africa/Niamey">Africa/Niamey</option>
                                                    <option value="Africa/Nouakchott">Africa/Nouakchott</option>
                                                    <option value="Africa/Ouagadougou">Africa/Ouagadougou</option>
                                                    <option value="Africa/Porto-Novo">Africa/Porto-Novo</option>
                                                    <option value="Africa/Sao_Tome">Africa/Sao_Tome</option>
                                                    <option value="Africa/Tripoli">Africa/Tripoli</option>
                                                    <option value="Africa/Tunis">Africa/Tunis</option>
                                                    <option value="Africa/Windhoek">Africa/Windhoek</option>
                                                    <option value="America/Adak">America/Adak</option>
                                                    <option value="America/Anchorage">America/Anchorage</option>
                                                    <option value="America/Anguilla">America/Anguilla</option>
                                                    <option value="America/Antigua">America/Antigua</option>
                                                    <option value="America/Araguaina">America/Araguaina</option>
                                                    <option value="America/Argentina/Buenos_Aires">
                                                        America/Argentina/Buenos_Aires</option>
                                                    <option value="America/Argentina/Catamarca">
                                                        America/Argentina/Catamarca</option>
                                                    <option value="America/Argentina/Cordoba">America/Argentina/Cordoba
                                                    </option>
                                                    <option value="America/Argentina/Jujuy">America/Argentina/Jujuy
                                                    </option>
                                                    <option value="America/Argentina/La_Rioja">
                                                        America/Argentina/La_Rioja</option>
                                                    <option value="America/Argentina/Mendoza">America/Argentina/Mendoza
                                                    </option>
                                                    <option value="America/Argentina/Rio_Gallegos">
                                                        America/Argentina/Rio_Gallegos</option>
                                                    <option value="America/Argentina/Salta">America/Argentina/Salta
                                                    </option>
                                                    <option value="America/Argentina/San_Juan">
                                                        America/Argentina/San_Juan</option>
                                                    <option value="America/Argentina/San_Luis">
                                                        America/Argentina/San_Luis</option>
                                                    <option value="America/Argentina/Tucuman">America/Argentina/Tucuman
                                                    </option>
                                                    <option value="America/Argentina/Ushuaia">America/Argentina/Ushuaia
                                                    </option>
                                                    <option value="America/Aruba">America/Aruba</option>
                                                    <option value="America/Asuncion">America/Asuncion</option>
                                                    <option value="America/Atikokan">America/Atikokan</option>
                                                    <option value="America/Bahia">America/Bahia</option>
                                                    <option value="America/Bahia_Banderas">America/Bahia_Banderas
                                                    </option>
                                                    <option value="America/Barbados">America/Barbados</option>
                                                    <option value="America/Belem">America/Belem</option>
                                                    <option value="America/Belize">America/Belize</option>
                                                    <option value="America/Blanc-Sablon">America/Blanc-Sablon</option>
                                                    <option value="America/Boa_Vista">America/Boa_Vista</option>
                                                    <option value="America/Bogota">America/Bogota</option>
                                                    <option value="America/Boise">America/Boise</option>
                                                    <option value="America/Cambridge_Bay">America/Cambridge_Bay</option>
                                                    <option value="America/Campo_Grande">America/Campo_Grande</option>
                                                    <option value="America/Cancun">America/Cancun</option>
                                                    <option value="America/Caracas">America/Caracas</option>
                                                    <option value="America/Cayenne">America/Cayenne</option>
                                                    <option value="America/Cayman">America/Cayman</option>
                                                    <option value="America/Chicago">America/Chicago</option>
                                                    <option value="America/Chihuahua">America/Chihuahua</option>
                                                    <option value="America/Costa_Rica">America/Costa_Rica</option>
                                                    <option value="America/Creston">America/Creston</option>
                                                    <option value="America/Cuiaba">America/Cuiaba</option>
                                                    <option value="America/Curacao">America/Curacao</option>
                                                    <option value="America/Danmarkshavn">America/Danmarkshavn</option>
                                                    <option value="America/Dawson">America/Dawson</option>
                                                    <option value="America/Dawson_Creek">America/Dawson_Creek</option>
                                                    <option value="America/Denver">America/Denver</option>
                                                    <option value="America/Detroit">America/Detroit</option>
                                                    <option value="America/Dominica">America/Dominica</option>
                                                    <option value="America/Edmonton">America/Edmonton</option>
                                                    <option value="America/Eirunepe">America/Eirunepe</option>
                                                    <option value="America/El_Salvador">America/El_Salvador</option>
                                                    <option value="America/Fort_Nelson">America/Fort_Nelson</option>
                                                    <option value="America/Fortaleza">America/Fortaleza</option>
                                                    <option value="America/Glace_Bay">America/Glace_Bay</option>
                                                    <option value="America/Godthab">America/Godthab</option>
                                                    <option value="America/Goose_Bay">America/Goose_Bay</option>
                                                    <option value="America/Grand_Turk">America/Grand_Turk</option>
                                                    <option value="America/Grenada">America/Grenada</option>
                                                    <option value="America/Guadeloupe">America/Guadeloupe</option>
                                                    <option value="America/Guatemala">America/Guatemala</option>
                                                    <option value="America/Guayaquil">America/Guayaquil</option>
                                                    <option value="America/Guyana">America/Guyana</option>
                                                    <option value="America/Halifax">America/Halifax</option>
                                                    <option value="America/Havana">America/Havana</option>
                                                    <option value="America/Hermosillo">America/Hermosillo</option>
                                                    <option value="America/Indiana/Indianapolis">
                                                        America/Indiana/Indianapolis</option>
                                                    <option value="America/Indiana/Knox">America/Indiana/Knox</option>
                                                    <option value="America/Indiana/Marengo">America/Indiana/Marengo
                                                    </option>
                                                    <option value="America/Indiana/Petersburg">
                                                        America/Indiana/Petersburg</option>
                                                    <option value="America/Indiana/Tell_City">America/Indiana/Tell_City
                                                    </option>
                                                    <option value="America/Indiana/Vevay">America/Indiana/Vevay</option>
                                                    <option value="America/Indiana/Vincennes">America/Indiana/Vincennes
                                                    </option>
                                                    <option value="America/Indiana/Winamac">America/Indiana/Winamac
                                                    </option>
                                                    <option value="America/Inuvik">America/Inuvik</option>
                                                    <option value="America/Iqaluit">America/Iqaluit</option>
                                                    <option value="America/Jamaica">America/Jamaica</option>
                                                    <option value="America/Juneau">America/Juneau</option>
                                                    <option value="America/Kentucky/Louisville">
                                                        America/Kentucky/Louisville</option>
                                                    <option value="America/Kentucky/Monticello">
                                                        America/Kentucky/Monticello</option>
                                                    <option value="America/Kralendijk">America/Kralendijk</option>
                                                    <option value="America/La_Paz">America/La_Paz</option>
                                                    <option value="America/Lima">America/Lima</option>
                                                    <option value="America/Los_Angeles">America/Los_Angeles</option>
                                                    <option value="America/Lower_Princes">America/Lower_Princes</option>
                                                    <option value="America/Maceio">America/Maceio</option>
                                                    <option value="America/Managua">America/Managua</option>
                                                    <option value="America/Manaus">America/Manaus</option>
                                                    <option value="America/Marigot">America/Marigot</option>
                                                    <option value="America/Martinique">America/Martinique</option>
                                                    <option value="America/Matamoros">America/Matamoros</option>
                                                    <option value="America/Mazatlan">America/Mazatlan</option>
                                                    <option value="America/Menominee">America/Menominee</option>
                                                    <option value="America/Merida">America/Merida</option>
                                                    <option value="America/Metlakatla">America/Metlakatla</option>
                                                    <option value="America/Mexico_City">America/Mexico_City</option>
                                                    <option value="America/Miquelon">America/Miquelon</option>
                                                    <option value="America/Moncton">America/Moncton</option>
                                                    <option value="America/Monterrey">America/Monterrey</option>
                                                    <option value="America/Montevideo">America/Montevideo</option>
                                                    <option value="America/Montserrat">America/Montserrat</option>
                                                    <option value="America/Nassau">America/Nassau</option>
                                                    <option value="America/New_York">America/New_York</option>
                                                    <option value="America/Nipigon">America/Nipigon</option>
                                                    <option value="America/Nome">America/Nome</option>
                                                    <option value="America/Noronha">America/Noronha</option>
                                                    <option value="America/North_Dakota/Beulah">
                                                        America/North_Dakota/Beulah</option>
                                                    <option value="America/North_Dakota/Center">
                                                        America/North_Dakota/Center</option>
                                                    <option value="America/North_Dakota/New_Salem">
                                                        America/North_Dakota/New_Salem</option>
                                                    <option value="America/Ojinaga">America/Ojinaga</option>
                                                    <option value="America/Panama">America/Panama</option>
                                                    <option value="America/Pangnirtung">America/Pangnirtung</option>
                                                    <option value="America/Paramaribo">America/Paramaribo</option>
                                                    <option value="America/Phoenix">America/Phoenix</option>
                                                    <option value="America/Port-au-Prince">America/Port-au-Prince
                                                    </option>
                                                    <option value="America/Port_of_Spain">America/Port_of_Spain</option>
                                                    <option value="America/Porto_Velho">America/Porto_Velho</option>
                                                    <option value="America/Puerto_Rico">America/Puerto_Rico</option>
                                                    <option value="America/Punta_Arenas">America/Punta_Arenas</option>
                                                    <option value="America/Rainy_River">America/Rainy_River</option>
                                                    <option value="America/Rankin_Inlet">America/Rankin_Inlet</option>
                                                    <option value="America/Recife">America/Recife</option>
                                                    <option value="America/Regina">America/Regina</option>
                                                    <option value="America/Resolute">America/Resolute</option>
                                                    <option value="America/Rio_Branco">America/Rio_Branco</option>
                                                    <option value="America/Santarem">America/Santarem</option>
                                                    <option value="America/Santiago">America/Santiago</option>
                                                    <option value="America/Santo_Domingo">America/Santo_Domingo</option>
                                                    <option value="America/Sao_Paulo">America/Sao_Paulo</option>
                                                    <option value="America/Scoresbysund">America/Scoresbysund</option>
                                                    <option value="America/Sitka">America/Sitka</option>
                                                    <option value="America/St_Barthelemy">America/St_Barthelemy</option>
                                                    <option value="America/St_Johns">America/St_Johns</option>
                                                    <option value="America/St_Kitts">America/St_Kitts</option>
                                                    <option value="America/St_Lucia">America/St_Lucia</option>
                                                    <option value="America/St_Thomas">America/St_Thomas</option>
                                                    <option value="America/St_Vincent">America/St_Vincent</option>
                                                    <option value="America/Swift_Current">America/Swift_Current</option>
                                                    <option value="America/Tegucigalpa">America/Tegucigalpa</option>
                                                    <option value="America/Thule">America/Thule</option>
                                                    <option value="America/Thunder_Bay">America/Thunder_Bay</option>
                                                    <option value="America/Tijuana">America/Tijuana</option>
                                                    <option value="America/Toronto">America/Toronto</option>
                                                    <option value="America/Tortola">America/Tortola</option>
                                                    <option value="America/Vancouver">America/Vancouver</option>
                                                    <option value="America/Whitehorse">America/Whitehorse</option>
                                                    <option value="America/Winnipeg">America/Winnipeg</option>
                                                    <option value="America/Yakutat">America/Yakutat</option>
                                                    <option value="America/Yellowknife">America/Yellowknife</option>
                                                    <option value="Antarctica/Casey">Antarctica/Casey</option>
                                                    <option value="Antarctica/Davis">Antarctica/Davis</option>
                                                    <option value="Antarctica/DumontDUrville">Antarctica/DumontDUrville
                                                    </option>
                                                    <option value="Antarctica/Macquarie">Antarctica/Macquarie</option>
                                                    <option value="Antarctica/Mawson">Antarctica/Mawson</option>
                                                    <option value="Antarctica/McMurdo">Antarctica/McMurdo</option>
                                                    <option value="Antarctica/Palmer">Antarctica/Palmer</option>
                                                    <option value="Antarctica/Rothera">Antarctica/Rothera</option>
                                                    <option value="Antarctica/Syowa">Antarctica/Syowa</option>
                                                    <option value="Antarctica/Troll">Antarctica/Troll</option>
                                                    <option value="Antarctica/Vostok">Antarctica/Vostok</option>
                                                    <option value="Arctic/Longyearbyen">Arctic/Longyearbyen</option>
                                                    <option value="Asia/Aden">Asia/Aden</option>
                                                    <option value="Asia/Almaty">Asia/Almaty</option>
                                                    <option value="Asia/Amman">Asia/Amman</option>
                                                    <option value="Asia/Anadyr">Asia/Anadyr</option>
                                                    <option value="Asia/Aqtau">Asia/Aqtau</option>
                                                    <option value="Asia/Aqtobe">Asia/Aqtobe</option>
                                                    <option value="Asia/Ashgabat">Asia/Ashgabat</option>
                                                    <option value="Asia/Atyrau">Asia/Atyrau</option>
                                                    <option value="Asia/Baghdad">Asia/Baghdad</option>
                                                    <option value="Asia/Bahrain">Asia/Bahrain</option>
                                                    <option value="Asia/Baku">Asia/Baku</option>
                                                    <option value="Asia/Bangkok">Asia/Bangkok</option>
                                                    <option value="Asia/Barnaul">Asia/Barnaul</option>
                                                    <option value="Asia/Beirut">Asia/Beirut</option>
                                                    <option value="Asia/Bishkek">Asia/Bishkek</option>
                                                    <option value="Asia/Brunei">Asia/Brunei</option>
                                                    <option value="Asia/Chita">Asia/Chita</option>
                                                    <option value="Asia/Choibalsan">Asia/Choibalsan</option>
                                                    <option value="Asia/Colombo">Asia/Colombo</option>
                                                    <option value="Asia/Damascus">Asia/Damascus</option>
                                                    <option value="Asia/Dhaka">Asia/Dhaka</option>
                                                    <option value="Asia/Dili">Asia/Dili</option>
                                                    <option value="Asia/Dubai">Asia/Dubai</option>
                                                    <option value="Asia/Dushanbe">Asia/Dushanbe</option>
                                                    <option value="Asia/Famagusta">Asia/Famagusta</option>
                                                    <option value="Asia/Gaza">Asia/Gaza</option>
                                                    <option value="Asia/Hebron">Asia/Hebron</option>
                                                    <option value="Asia/Ho_Chi_Minh">Asia/Ho_Chi_Minh</option>
                                                    <option value="Asia/Hong_Kong">Asia/Hong_Kong</option>
                                                    <option value="Asia/Hovd">Asia/Hovd</option>
                                                    <option value="Asia/Irkutsk">Asia/Irkutsk</option>
                                                    <option value="Asia/Jakarta">Asia/Jakarta</option>
                                                    <option value="Asia/Jayapura">Asia/Jayapura</option>
                                                    <option value="Asia/Jerusalem">Asia/Jerusalem</option>
                                                    <option value="Asia/Kabul">Asia/Kabul</option>
                                                    <option value="Asia/Kamchatka">Asia/Kamchatka</option>
                                                    <option value="Asia/Karachi">Asia/Karachi</option>
                                                    <option value="Asia/Kathmandu">Asia/Kathmandu</option>
                                                    <option value="Asia/Khandyga">Asia/Khandyga</option>
                                                    <option value="Asia/Kolkata" selected="selected">Asia/Kolkata
                                                    </option>
                                                    <option value="Asia/Krasnoyarsk">Asia/Krasnoyarsk</option>
                                                    <option value="Asia/Kuala_Lumpur">Asia/Kuala_Lumpur</option>
                                                    <option value="Asia/Kuching">Asia/Kuching</option>
                                                    <option value="Asia/Kuwait">Asia/Kuwait</option>
                                                    <option value="Asia/Macau">Asia/Macau</option>
                                                    <option value="Asia/Magadan">Asia/Magadan</option>
                                                    <option value="Asia/Makassar">Asia/Makassar</option>
                                                    <option value="Asia/Manila">Asia/Manila</option>
                                                    <option value="Asia/Muscat">Asia/Muscat</option>
                                                    <option value="Asia/Nicosia">Asia/Nicosia</option>
                                                    <option value="Asia/Novokuznetsk">Asia/Novokuznetsk</option>
                                                    <option value="Asia/Novosibirsk">Asia/Novosibirsk</option>
                                                    <option value="Asia/Omsk">Asia/Omsk</option>
                                                    <option value="Asia/Oral">Asia/Oral</option>
                                                    <option value="Asia/Phnom_Penh">Asia/Phnom_Penh</option>
                                                    <option value="Asia/Pontianak">Asia/Pontianak</option>
                                                    <option value="Asia/Pyongyang">Asia/Pyongyang</option>
                                                    <option value="Asia/Qatar">Asia/Qatar</option>
                                                    <option value="Asia/Qostanay">Asia/Qostanay</option>
                                                    <option value="Asia/Qyzylorda">Asia/Qyzylorda</option>
                                                    <option value="Asia/Riyadh">Asia/Riyadh</option>
                                                    <option value="Asia/Sakhalin">Asia/Sakhalin</option>
                                                    <option value="Asia/Samarkand">Asia/Samarkand</option>
                                                    <option value="Asia/Seoul">Asia/Seoul</option>
                                                    <option value="Asia/Shanghai">Asia/Shanghai</option>
                                                    <option value="Asia/Singapore">Asia/Singapore</option>
                                                    <option value="Asia/Srednekolymsk">Asia/Srednekolymsk</option>
                                                    <option value="Asia/Taipei">Asia/Taipei</option>
                                                    <option value="Asia/Tashkent">Asia/Tashkent</option>
                                                    <option value="Asia/Tbilisi">Asia/Tbilisi</option>
                                                    <option value="Asia/Tehran">Asia/Tehran</option>
                                                    <option value="Asia/Thimphu">Asia/Thimphu</option>
                                                    <option value="Asia/Tokyo">Asia/Tokyo</option>
                                                    <option value="Asia/Tomsk">Asia/Tomsk</option>
                                                    <option value="Asia/Ulaanbaatar">Asia/Ulaanbaatar</option>
                                                    <option value="Asia/Urumqi">Asia/Urumqi</option>
                                                    <option value="Asia/Ust-Nera">Asia/Ust-Nera</option>
                                                    <option value="Asia/Vientiane">Asia/Vientiane</option>
                                                    <option value="Asia/Vladivostok">Asia/Vladivostok</option>
                                                    <option value="Asia/Yakutsk">Asia/Yakutsk</option>
                                                    <option value="Asia/Yangon">Asia/Yangon</option>
                                                    <option value="Asia/Yekaterinburg">Asia/Yekaterinburg</option>
                                                    <option value="Asia/Yerevan">Asia/Yerevan</option>
                                                    <option value="Atlantic/Azores">Atlantic/Azores</option>
                                                    <option value="Atlantic/Bermuda">Atlantic/Bermuda</option>
                                                    <option value="Atlantic/Canary">Atlantic/Canary</option>
                                                    <option value="Atlantic/Cape_Verde">Atlantic/Cape_Verde</option>
                                                    <option value="Atlantic/Faroe">Atlantic/Faroe</option>
                                                    <option value="Atlantic/Madeira">Atlantic/Madeira</option>
                                                    <option value="Atlantic/Reykjavik">Atlantic/Reykjavik</option>
                                                    <option value="Atlantic/South_Georgia">Atlantic/South_Georgia
                                                    </option>
                                                    <option value="Atlantic/St_Helena">Atlantic/St_Helena</option>
                                                    <option value="Atlantic/Stanley">Atlantic/Stanley</option>
                                                    <option value="Australia/Adelaide">Australia/Adelaide</option>
                                                    <option value="Australia/Brisbane">Australia/Brisbane</option>
                                                    <option value="Australia/Broken_Hill">Australia/Broken_Hill</option>
                                                    <option value="Australia/Currie">Australia/Currie</option>
                                                    <option value="Australia/Darwin">Australia/Darwin</option>
                                                    <option value="Australia/Eucla">Australia/Eucla</option>
                                                    <option value="Australia/Hobart">Australia/Hobart</option>
                                                    <option value="Australia/Lindeman">Australia/Lindeman</option>
                                                    <option value="Australia/Lord_Howe">Australia/Lord_Howe</option>
                                                    <option value="Australia/Melbourne">Australia/Melbourne</option>
                                                    <option value="Australia/Perth">Australia/Perth</option>
                                                    <option value="Australia/Sydney">Australia/Sydney</option>
                                                    <option value="Europe/Amsterdam">Europe/Amsterdam</option>
                                                    <option value="Europe/Andorra">Europe/Andorra</option>
                                                    <option value="Europe/Astrakhan">Europe/Astrakhan</option>
                                                    <option value="Europe/Athens">Europe/Athens</option>
                                                    <option value="Europe/Belgrade">Europe/Belgrade</option>
                                                    <option value="Europe/Berlin">Europe/Berlin</option>
                                                    <option value="Europe/Bratislava">Europe/Bratislava</option>
                                                    <option value="Europe/Brussels">Europe/Brussels</option>
                                                    <option value="Europe/Bucharest">Europe/Bucharest</option>
                                                    <option value="Europe/Budapest">Europe/Budapest</option>
                                                    <option value="Europe/Busingen">Europe/Busingen</option>
                                                    <option value="Europe/Chisinau">Europe/Chisinau</option>
                                                    <option value="Europe/Copenhagen">Europe/Copenhagen</option>
                                                    <option value="Europe/Dublin">Europe/Dublin</option>
                                                    <option value="Europe/Gibraltar">Europe/Gibraltar</option>
                                                    <option value="Europe/Guernsey">Europe/Guernsey</option>
                                                    <option value="Europe/Helsinki">Europe/Helsinki</option>
                                                    <option value="Europe/Isle_of_Man">Europe/Isle_of_Man</option>
                                                    <option value="Europe/Istanbul">Europe/Istanbul</option>
                                                    <option value="Europe/Jersey">Europe/Jersey</option>
                                                    <option value="Europe/Kaliningrad">Europe/Kaliningrad</option>
                                                    <option value="Europe/Kiev">Europe/Kiev</option>
                                                    <option value="Europe/Kirov">Europe/Kirov</option>
                                                    <option value="Europe/Lisbon">Europe/Lisbon</option>
                                                    <option value="Europe/Ljubljana">Europe/Ljubljana</option>
                                                    <option value="Europe/London">Europe/London</option>
                                                    <option value="Europe/Luxembourg">Europe/Luxembourg</option>
                                                    <option value="Europe/Madrid">Europe/Madrid</option>
                                                    <option value="Europe/Malta">Europe/Malta</option>
                                                    <option value="Europe/Mariehamn">Europe/Mariehamn</option>
                                                    <option value="Europe/Minsk">Europe/Minsk</option>
                                                    <option value="Europe/Monaco">Europe/Monaco</option>
                                                    <option value="Europe/Moscow">Europe/Moscow</option>
                                                    <option value="Europe/Oslo">Europe/Oslo</option>
                                                    <option value="Europe/Paris">Europe/Paris</option>
                                                    <option value="Europe/Podgorica">Europe/Podgorica</option>
                                                    <option value="Europe/Prague">Europe/Prague</option>
                                                    <option value="Europe/Riga">Europe/Riga</option>
                                                    <option value="Europe/Rome">Europe/Rome</option>
                                                    <option value="Europe/Samara">Europe/Samara</option>
                                                    <option value="Europe/San_Marino">Europe/San_Marino</option>
                                                    <option value="Europe/Sarajevo">Europe/Sarajevo</option>
                                                    <option value="Europe/Saratov">Europe/Saratov</option>
                                                    <option value="Europe/Simferopol">Europe/Simferopol</option>
                                                    <option value="Europe/Skopje">Europe/Skopje</option>
                                                    <option value="Europe/Sofia">Europe/Sofia</option>
                                                    <option value="Europe/Stockholm">Europe/Stockholm</option>
                                                    <option value="Europe/Tallinn">Europe/Tallinn</option>
                                                    <option value="Europe/Tirane">Europe/Tirane</option>
                                                    <option value="Europe/Ulyanovsk">Europe/Ulyanovsk</option>
                                                    <option value="Europe/Uzhgorod">Europe/Uzhgorod</option>
                                                    <option value="Europe/Vaduz">Europe/Vaduz</option>
                                                    <option value="Europe/Vatican">Europe/Vatican</option>
                                                    <option value="Europe/Vienna">Europe/Vienna</option>
                                                    <option value="Europe/Vilnius">Europe/Vilnius</option>
                                                    <option value="Europe/Volgograd">Europe/Volgograd</option>
                                                    <option value="Europe/Warsaw">Europe/Warsaw</option>
                                                    <option value="Europe/Zagreb">Europe/Zagreb</option>
                                                    <option value="Europe/Zaporozhye">Europe/Zaporozhye</option>
                                                    <option value="Europe/Zurich">Europe/Zurich</option>
                                                    <option value="Indian/Antananarivo">Indian/Antananarivo</option>
                                                    <option value="Indian/Chagos">Indian/Chagos</option>
                                                    <option value="Indian/Christmas">Indian/Christmas</option>
                                                    <option value="Indian/Cocos">Indian/Cocos</option>
                                                    <option value="Indian/Comoro">Indian/Comoro</option>
                                                    <option value="Indian/Kerguelen">Indian/Kerguelen</option>
                                                    <option value="Indian/Mahe">Indian/Mahe</option>
                                                    <option value="Indian/Maldives">Indian/Maldives</option>
                                                    <option value="Indian/Mauritius">Indian/Mauritius</option>
                                                    <option value="Indian/Mayotte">Indian/Mayotte</option>
                                                    <option value="Indian/Reunion">Indian/Reunion</option>
                                                    <option value="Pacific/Apia">Pacific/Apia</option>
                                                    <option value="Pacific/Auckland">Pacific/Auckland</option>
                                                    <option value="Pacific/Bougainville">Pacific/Bougainville</option>
                                                    <option value="Pacific/Chatham">Pacific/Chatham</option>
                                                    <option value="Pacific/Chuuk">Pacific/Chuuk</option>
                                                    <option value="Pacific/Easter">Pacific/Easter</option>
                                                    <option value="Pacific/Efate">Pacific/Efate</option>
                                                    <option value="Pacific/Enderbury">Pacific/Enderbury</option>
                                                    <option value="Pacific/Fakaofo">Pacific/Fakaofo</option>
                                                    <option value="Pacific/Fiji">Pacific/Fiji</option>
                                                    <option value="Pacific/Funafuti">Pacific/Funafuti</option>
                                                    <option value="Pacific/Galapagos">Pacific/Galapagos</option>
                                                    <option value="Pacific/Gambier">Pacific/Gambier</option>
                                                    <option value="Pacific/Guadalcanal">Pacific/Guadalcanal</option>
                                                    <option value="Pacific/Guam">Pacific/Guam</option>
                                                    <option value="Pacific/Honolulu">Pacific/Honolulu</option>
                                                    <option value="Pacific/Kiritimati">Pacific/Kiritimati</option>
                                                    <option value="Pacific/Kosrae">Pacific/Kosrae</option>
                                                    <option value="Pacific/Kwajalein">Pacific/Kwajalein</option>
                                                    <option value="Pacific/Majuro">Pacific/Majuro</option>
                                                    <option value="Pacific/Marquesas">Pacific/Marquesas</option>
                                                    <option value="Pacific/Midway">Pacific/Midway</option>
                                                    <option value="Pacific/Nauru">Pacific/Nauru</option>
                                                    <option value="Pacific/Niue">Pacific/Niue</option>
                                                    <option value="Pacific/Norfolk">Pacific/Norfolk</option>
                                                    <option value="Pacific/Noumea">Pacific/Noumea</option>
                                                    <option value="Pacific/Pago_Pago">Pacific/Pago_Pago</option>
                                                    <option value="Pacific/Palau">Pacific/Palau</option>
                                                    <option value="Pacific/Pitcairn">Pacific/Pitcairn</option>
                                                    <option value="Pacific/Pohnpei">Pacific/Pohnpei</option>
                                                    <option value="Pacific/Port_Moresby">Pacific/Port_Moresby</option>
                                                    <option value="Pacific/Rarotonga">Pacific/Rarotonga</option>
                                                    <option value="Pacific/Saipan">Pacific/Saipan</option>
                                                    <option value="Pacific/Tahiti">Pacific/Tahiti</option>
                                                    <option value="Pacific/Tarawa">Pacific/Tarawa</option>
                                                    <option value="Pacific/Tongatapu">Pacific/Tongatapu</option>
                                                    <option value="Pacific/Wake">Pacific/Wake</option>
                                                    <option value="Pacific/Wallis">Pacific/Wallis</option>
                                                    <option value="UTC">UTC</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="no" style="display:none;">
                                    <div class="form-group">
                                        <?= lang('biller', 'biller'); ?>
                                        <?php
                                                    $bl[''] = lang('select') . ' ' . lang('biller');
                                                    foreach ($billers as $biller) {
                                                        $bl[$biller->id] = $biller->company && $biller->company != '-' ? $biller->company : $biller->name;
                                                    }
                                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ''), 'id="biller" class="form-control select" style="width:100%;"');
                                                    ?>
                                    </div>

                                    <div class="form-group">
                                        <?= lang('warehouse', 'warehouse'); ?>
                                        <?php
                                                    $wh[''] = lang('select') . ' ' . lang('warehouse');
                                                    foreach ($warehouses as $warehouse) {
                                                        $wh[$warehouse->id] = $warehouse->name;
                                                    }
                                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ''), 'id="warehouse" class="form-control select" style="width:100%;" ');
                                                    ?>
                                    </div>

                                    <div class="form-group">
                                        <?= lang('view_right', 'view_right'); ?>
                                        <?php
                                                    $vropts = [1 => lang('all_records'), 0 => lang('own_records')];
                                                    echo form_dropdown('view_right', $vropts, (isset($_POST['view_right']) ? $_POST['view_right'] : 1), 'id="view_right" class="form-control select" style="width:100%;"');
                                                    ?>
                                    </div>
                                    <div class="form-group">
                                        <?= lang('edit_right', 'edit_right'); ?>
                                        <?php
                                                    $opts = [1 => lang('yes'), 0 => lang('no')];
                                                    echo form_dropdown('edit_right', $opts, (isset($_POST['edit_right']) ? $_POST['edit_right'] : 0), 'id="edit_right" class="form-control select" style="width:100%;"');
                                                    ?>
                                    </div>
                                    <div class="form-group">
                                        <?= lang('allow_discount', 'allow_discount'); ?>
                                        <?= form_dropdown('allow_discount', $opts, (isset($_POST['allow_discount']) ? $_POST['allow_discount'] : 0), 'id="allow_discount" class="form-control select" style="width:100%;"'); ?>
                                    </div>
                                </div>



                                <!-- tax details -->
                                <h3>Business Settings</h3>

                                <fieldset>
                                    <legend>Business Settings:</legend>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="tax_label_1">Tax 1 Name:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-info"></i>
                                                </span>
                                                <input class="form-control" placeholder="GST / VAT / Other"
                                                    name="tax_label_1" type="text" id="tax_label_1">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="tax_number_1">Tax 1 No.:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-info"></i>
                                                </span>
                                                <input class="form-control" name="tax_number_1" type="text"
                                                    id="tax_number_1">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="tax_label_2">Tax 2 Name:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-info"></i>
                                                </span>
                                                <input class="form-control" placeholder="GST / VAT / Other"
                                                    name="tax_label_2" type="text" id="tax_label_2">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="tax_number_2">Tax 2 No.:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-info"></i>
                                                </span>
                                                <input class="form-control" name="tax_number_2" type="text"
                                                    id="tax_number_2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fy_start_month">Financial year start month:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                                <select class="form-control select2_register" required
                                                    style="width:100%;" id="fy_start_month" name="fy_start_month">
                                                    <option value="1">January</option>
                                                    <option value="2">February</option>
                                                    <option value="3">March</option>
                                                    <option value="4">April</option>
                                                    <option value="5">May</option>
                                                    <option value="6">June</option>
                                                    <option value="7">July</option>
                                                    <option value="8">August</option>
                                                    <option value="9">September</option>
                                                    <option value="10">October</option>
                                                    <option value="11">November</option>
                                                    <option value="12">December</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="accounting_method">Stock Accounting Method:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calculator"></i>
                                                </span>
                                                <select class="form-control select2_register" required
                                                    style="width:100%;" id="accounting_method" name="accounting_method">
                                                    <option value="fifo">FIFO (First In First Out)</option>
                                                    <option value="lifo">LIFO (Last In First Out)</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                                <!-- Owner Information -->
                                <h3>Owner</h3>

                                <fieldset>
                                    <legend>Owner information</legend>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="surname">Prefix:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-info"></i>
                                                </span>
                                                <input class="form-control" placeholder="Mr / Mrs / Miss" name="surname"
                                                    type="text" id="surname">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="first_name">First Name:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-info"></i>
                                                </span>
                                                <input class="form-control" placeholder="First Name" required
                                                    name="first_name" type="text" id="first_name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="last_name">Last Name:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-info"></i>
                                                </span>
                                                <input class="form-control" placeholder="Last Name" name="last_name"
                                                    type="text" id="last_name">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="username">Username:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-user"></i>
                                                </span>
                                                <input class="form-control" placeholder="Username" required
                                                    name="username" type="text" id="username">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-envelope"></i>
                                                </span>
                                                <input class="form-control" placeholder="Email" name="email" type="text"
                                                    id="email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password">Password:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-lock"></i>
                                                </span>
                                                <input class="form-control" placeholder="Password" required
                                                    name="password" type="password" value="" id="password">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="confirm_password">Confirm Password:</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-lock"></i>
                                                </span>
                                                <input class="form-control" placeholder="Confirm Password" required
                                                    name="confirm_password" type="password" value=""
                                                    id="confirm_password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-6">
                                    </div>
                                    <div class="clearfix"></div>
                                </fieldset> <input name="package_id" type="hidden">








                                <?php echo form_close(); ?>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
    base_path = "<?= base_url().'/' ?>";
    </script>

    <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js?v=$asset_v"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js?v=$asset_v"></script>
<![endif]-->

    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/pace/pace.min.js?v=36"></script>

    <!-- jQuery 2.2.3 -->
    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/jQuery/jquery-2.2.3.min.js?v=36"></script>
    <script src="<?= base_url().'assets/register/' ?>plugins/jquery-ui/jquery-ui.min.js?v=36"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?= base_url().'assets/register/' ?>bootstrap/js/bootstrap.min.js?v=36"></script>
    <!-- iCheck -->
    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/iCheck/icheck.min.js?v=36"></script>
    <!-- Select2 -->
    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/select2/select2.full.min.js?v=36"></script>
    <!-- Add language file for select2 -->
    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/select2/lang/en.js?v=36"></script>
    <!-- bootstrap datepicker -->
    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/datepicker/bootstrap-datepicker.min.js?v=36"></script>
    <!-- DataTables -->
    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/DataTables/datatables.min.js?v=36"></script>
    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/DataTables/pdfmake-0.1.32/pdfmake.min.js?v=36">
    </script>
    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/DataTables/pdfmake-0.1.32/vfs_fonts.js?v=36"></script>

    <!-- jQuery Validator -->
    <script src="<?= base_url().'assets/register/' ?>js/jquery-validation-1.16.0/dist/jquery.validate.min.js?v=36"></script>
    <script src="<?= base_url().'assets/register/' ?>js/jquery-validation-1.16.0/dist/additional-methods.min.js?v=36">
    </script>

    <!-- Toastr -->
    <script src="<?= base_url().'assets/register/' ?>plugins/toastr/toastr.min.js?v=36"></script>
    <!-- Bootstrap file input -->
    <script src="<?= base_url().'assets/register/' ?>plugins/bootstrap-fileinput/fileinput.min.js?v=36"></script>
    <!--accounting js-->
    <script src="<?= base_url().'assets/register/' ?>plugins/accounting.min.js?v=36"></script>

    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/daterangepicker/moment.min.js?v=36"></script>

    <script src="<?= base_url().'assets/register/' ?>plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js?v=36">
    </script>

    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/daterangepicker/daterangepicker.js?v=36"></script>

    <script src="<?= base_url().'assets/register/' ?>AdminLTE/plugins/ckeditor/ckeditor.js?v=36"></script>

    <script src="<?= base_url().'assets/register/' ?>plugins/sweetalert/sweetalert.min.js?v=36"></script>

    <script src="<?= base_url().'assets/register/' ?>plugins/bootstrap-tour/bootstrap-tour.min.js?v=36"></script>

    <script src="<?= base_url().'assets/register/' ?>plugins/printThis.js?v=36"></script>

    <script src="<?= base_url().'assets/register/' ?>plugins/screenfull.min.js?v=36"></script>

    <script src="<?= base_url().'assets/register/' ?>plugins/moment-timezone-with-data.min.js?v=36 "></script>
    <script>
    moment.tz.setDefault('');
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token "]').attr('content')
            }
        });

        $.fn.dataTable.ext.errMode = 'throw';
    });

    var financial_year = {
        start: moment(''),
        end: moment(''),
    }
    //Default setting for select2
    $.fn.select2.defaults.set("language ", "en ");

    var datepicker_date_format = " ";
    var moment_date_format = " ";
    var moment_time_format = "HH:mm ";

    var app_locale = "en ";
    var non_utf8_languages = [
        "ar ",
        "hi ",
        "ps ",
    ];
    </script>

    <!-- Scripts -->
    <script src="<?= base_url().'assets/register/' ?>js/AdminLTE-app.js?v=36 "></script>

    <script src="<?= base_url().'assets/register/' ?>js/lang/en.js?v=36 "></script>

    <script src="<?= base_url().'assets/register/' ?>js/functions.js?v=36 "></script>
    <script src="<?= base_url().'assets/register/' ?>js/common.js?v=36 "></script>
    <script src="<?= base_url().'assets/register/' ?>js/app.js?v=36 "></script>
    <script src="<?= base_url().'assets/register/' ?>js/help-tour.js?v=36 "></script>
    <script src="<?= base_url().'assets/register/' ?>plugins/calculator/calculator.js?v=36 "></script>
    <script type="text/javascript ">
    $(document).ready(function() {
        $('#change_lang').change(function() {
            window.location = "http://localhost/mypos/full_code/mypos/admin/register?lang=" + $(this)
                .val();
        });
        // $(" a[href='#finish' ] ").href(login.html);
    })
    </script>

    <script src=" <?= base_url().'assets/register/' ?>plugins/jquery.steps/jquery.steps.min.js?v=36 "></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
        integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="<?= base_url().'assets/register/' ?>js/login.js?v=36 "></script>
    <script type="text/javascript ">
    $(document).ready(function() {
        $('#change_lang').change(function() {
            window.location = "http://localhost/mypos/full_code/mypos/admin/register?lang=" + $(this)
                .val();
        });
        //$('a[href="#finish"]').click(function(e){
        //	e.preventDefault();
        //window.location.replace("login.html");
        //	alert(12);
        //	$('#business_register_form').after("<form action='login.html' method='post'><input name='formdata' value='"+JSON.stringify($('#business_register_form').serializeArray())+"'></form>");
        //});
        $('a[href="#finish"]').click(function(e) {
            if ($("input.error").length === 0) {
                $('#business_register_form').off('submit');
                $('#business_register_form').submit();
            }
        });
    })
    </script>

    <!--script>
        $(" a[href='#finish' ] ").click(function(e){
            e.preventDefault();
            window.location.replace("login.html ");
        });
    </script-->
</body>

</html>