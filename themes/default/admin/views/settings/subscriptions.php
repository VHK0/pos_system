<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script
    src="https://www.paypal.com/sdk/js?client-id=AakYduU_4GXtTAtUABF_WXSIfMiQs8VYzDs6RdV2az-3ciEhM2osyCF747Cy0xYEJxsuVvtcS0Lfm2dj"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>
<style>
	.bss {
		background-color:#fff;
		font-weight: bold;
		border: 1px solid #d7d8d9;
		padding: 10px;
	}
	.titles {
		color:#1ed71e;
		font-size: 17px;
	}
	.subs {
		background-color: #dfa51c;
		padding:15px;
		color:#fff;
		font-weight: bold;
	}
	.subs span {
		float:right;
	}
	.img {
		display:block;
		margin:auto;
	}
</style>
<div class="box" style="margin-bottom: 15px;">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-database"></i>Subscription Status</h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-lg-4">
						<?php 
							
							$active = $subs['active'];
							if($active=="0") {
								$act = "Trial";
							} elseif($active=="1") {
								$act = "Yearly";
							} else {
								$act="Expired";
							}
							$start = date('Y-m-d',strtotime($subs['subs_start']));
							$today = date('Y-m-d');
							$days = $subs['subs_period'];
							$end_date = date('Y-m-d', strtotime($subs_start. ' +'.$days.' days'));
							$date1 = new DateTime($end_date);
							$date2 = new DateTime($start);
							//$dayss  = $date1->diff($date2)->format('%a');
							$dayss = (strtotime($end_date) - strtotime($start)) / (60 * 60 * 24);
							$finalD = $dayss-$days;
							$finalDas = $days-$finalD;
							
							
						?>
                       <div class="bss">
							<div class="titles">
								<center>
									<p>Subscription : <?php echo $act ?> (Active)</p>
									<p>Subscription Date : <?php echo $start?></p>
									<p>Remaining Days : <?php echo $finalDas;?></p>
								</center>
							</div>
							<div class="subs">Subscription <span class="text-right">Ksh 1,000.00</span></div>
							<h6>First time subscription is for one year and Price is 30,000 KES  [296 USD] . </h6>
					<h6>For all rest of years subscription fee per year is 10,000 KES [99 USD]</h6>
							<center><h1><b>Pay Via</b></h1></center>
							<p><a href="<?php echo base_url('admin/paypal/pay') ?>"><img src="<?php echo base_url('assets/images/paypal.jpg'); ?>" class="img" /></a></p>
							<div id="paypal-button-container"></div>
					   </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  /*paypal.Buttons({
    createOrder: function(data, actions) {
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: '0.01'
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      return actions.order.capture().then(function(details) {
        alert('Transaction completed by ' + details.payer.name.given_name);
        console.log(data);
      });
    }
  }).render('#paypal-button-container');*/
</script>
