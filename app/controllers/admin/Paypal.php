<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Paypal extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct();
		$this->load->library('paypal_lib');
    }
    
    public function pay()
    {
		$ammount =0.0;  
		$ownerid = $_SESSION['user_id'];
		$qa = "select * from sma_subscriptions where owner_id = $ownerid";
		$ownerrr = $this->db->query($qa);
		
		if( $ownerrr->num_rows() > 0 ) {
			// it is business owner 
			$res = $ownerrr->row_array();
			$status = $res['status'];  
			if($status=="0") {
				$period = "365";
				$ammount = 299.00; // equals 30000 kenya shillings 
			} elseif($status=="1") {
				$period = "365";
				$ammount = 99.00; // equals 10000 kenya shillings 
			}
		} else {
			/*$owner = $_SESSION['user_id'];
			$today	= date('Y-m-d H:i:s');
			$arr = array(
				'owner_id'		=> $owner,
				'type'			=> 'trial',
				'amount'		=> '0.00',
				'trxid'			=> '',
				'description'	=> 'Trial subscription period of the business',
				'payeer_email'	=> '',
				'subs_start'	=> $today,
				'subs_period'	=> 30, // shows number of days offer is active
				'date_created'	=> date('Y-m-d H:i:s'),
				'active'		=> '0' // 0 trial, 1 active, 2 expired
			);
			$this->db->insert('sma_subscriptions',$arr);
			$ammount = 0.00; */
		}
		
		$qty=1; 
		// Set variables for paypal form
		$returnURL = base_url('admin/paypal/').'paypal_response?success=true';
		$notify_url = base_url('admin/paypal/').'paypal_response?success=true';
		$cancelURL = base_url('admin/paypal/').'paypal_response?success=false';
		
		
		
		// Add fields to paypal form
		$this->paypal_lib->add_field('return', $returnURL);
		$this->paypal_lib->add_field('cancel_return', $cancelURL);
		$this->paypal_lib->add_field('notify_url', $notify_url);
		$this->paypal_lib->add_field('success_url', $returnUrl);
		$this->paypal_lib->add_field('item_name', 'subscription');
		$this->paypal_lib->add_field('custom', '1');
		$this->paypal_lib->add_field('item_number',  11);
		$this->paypal_lib->add_field('amount', $ammount);
		$this->paypal_lib->add_field('quantity',  $qty);
		
		// Render paypal form
		$this->paypal_lib->paypal_auto_form();
    }
	
	
    public function paypal_response()
    {
		
		if($_GET['success']==true) {
			
			$owner = $_SESSION['user_id'];
			$arr = array(
				'trxid'		=> $_POST['txn_id'],
				'type'		=> 'Yearly',
				'amount'	=> $_POST['payment_gross'],
				'description' => 'Yearly subscription',
				'payeer_email'=> $_POST['payer_email'],
				'subs_start'  => date('Y-m-d',strtotime($_POST['payment_date'])),
				'subs_period' => '365', // year in days
				'active'	  => '1',
				'status'	  => '1',
			);
		  $this->db->where('owner_id', $owner);
		  $res = $this->db->update('sma_subscriptions',$arr);
		  $referrer = ($this->session->userdata('requested_page') && $this->session->userdata('requested_page') != 'admin') ? $this->session->userdata('requested_page') : 'User subscribed successfully.';
		  admin_redirect($referrer);
		  
		} elseif($_GET['success']==false) {
			 
				$this->load->view($this->theme . 'default/paypal_error');
			
		}
    }
	
	public function ipn() {
		
		var_dump($_POST);
	}
}
?>